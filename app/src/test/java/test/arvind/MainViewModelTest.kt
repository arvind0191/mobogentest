package test.arvind

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import test.arvind.repository.domain.Book
import test.arvind.repository.domain.Category
import test.arvind.repository.domain.Character
import test.arvind.repository.domain.House
import test.arvind.repository.network.ApiResult
import test.arvind.repository.network.MoboRepository
import test.arvind.sharedPrefs.SharedPrefs
import test.arvind.feature.book.BooksViewState
import test.arvind.feature.category.CategoryViewState
import test.arvind.feature.character.CharacterViewState
import test.arvind.feature.house.HouseViewState

@ExperimentalCoroutinesApi
class MainViewModelTest {

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()

    private var repository: MoboRepository = mock()
    private var sharedPrefs: SharedPrefs = mock()
    private val mainViewModel = MainViewModel(repository, sharedPrefs)

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun onCategoryFragmentLoadedTest() {
        val categoryObserver: Observer<CategoryViewState> = mock()
        val categories: List<Category> = mock()
        whenever(sharedPrefs.getCategory()).thenReturn(categories)
        mainViewModel.categoryLiveData.observeForever(categoryObserver)
        mainViewModel.onCategoryFragmentLoaded()

        argumentCaptor<CategoryViewState> {
            verify(sharedPrefs).getCategory()
            verifyNoMoreInteractions(sharedPrefs)
        }
    }

    @Test
    fun onBooksFragmentLoadedTest() {
        val booksObserver: Observer<BooksViewState> = mock()
        val books: List<Book> = mock()
        testDispatcher.runBlockingTest {
            whenever(repository.fetchBooks()).thenReturn(ApiResult.Success(books))
            mainViewModel.onBooksFragmentLoaded()
            mainViewModel.bookLiveData.observeForever(booksObserver)

            argumentCaptor<BooksViewState> {
                verify(repository).fetchBooks()
                verifyNoMoreInteractions(sharedPrefs)
            }
        }
    }

    @Test
    fun onCharFragmentLoadedTest() {
        val characterObserver: Observer<CharacterViewState> = mock()

        val characters: List<Character> = mock()
        testDispatcher.runBlockingTest {
            doReturn(characters).`when`(repository).fetchChar()
            mainViewModel.onCategoryFragmentLoaded()
            mainViewModel.charLiveData.observeForever(characterObserver)
            verify(characterObserver).onChanged(CharacterViewState.Success(characters))
        }
    }

    @Test
    fun onHouseFragmentLoadedTest() {
        val houseObserver: Observer<HouseViewState> = mock()

        val houses :List<House> = mock()
        testDispatcher.runBlockingTest {
            doReturn(houses).`when`(repository).fetchHouses()
            mainViewModel.onCategoryFragmentLoaded()
            mainViewModel.houseLiveData.observeForever(houseObserver)
            verify(houseObserver).onChanged(HouseViewState.Success(houses))
        }
    }
}