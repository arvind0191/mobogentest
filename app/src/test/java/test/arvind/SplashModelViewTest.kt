package test.arvind

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import test.arvind.feature.splashscreen.SplashScreenViewModel
import test.arvind.repository.domain.Category
import test.arvind.repository.network.ApiResult
import test.arvind.repository.network.MoboRepository
import test.arvind.sharedPrefs.SharedPrefs

@ExperimentalCoroutinesApi
class SplashModelViewTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val testDispatcher = TestCoroutineDispatcher()
    private var sharedPrefs: SharedPrefs = mock()
    private var repository: MoboRepository = mock()
    private val splashViewModel = SplashScreenViewModel(repository, sharedPrefs)

    @Test
    fun onCategoryFragmentLoadedTest() {
        val categoryObserver: Observer<Unit> = mock()
        val categories: List<Category> = mock()
        testDispatcher.runBlockingTest {
            whenever(repository.fetchCategory()).thenReturn(ApiResult.Success(categories))
            whenever(sharedPrefs.hasCategory()).thenReturn(false)
            splashViewModel.onSplashScreenStart()
            splashViewModel.categorySavedTrigger.observeForever(categoryObserver)

            argumentCaptor<Unit> {
                verify(repository).fetchCategory()
                Assert.assertEquals(splashViewModel.categorySavedTrigger.value, firstValue)
                verifyNoMoreInteractions(repository)
                verifyNoMoreInteractions(sharedPrefs)
            }
        }
    }
}