package test.arvind

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import test.arvind.MoboApplication.Companion.sharedPrefs
import test.arvind.feature.book.BookFragment
import test.arvind.feature.category.CategoryFragment
import test.arvind.feature.character.CharacterFragment
import test.arvind.feature.house.HouseFragment
import test.arvind.feature.showErrorDialog

class MainActivity : AppCompatActivity() {
    private val mainViewModel by lazy {
        getMainViewModel(
            this,
            CommonDependencies.repository,
            sharedPrefs
        )
    }

    private val onViewStateChanged = Observer<MainViewState> { state ->
        when (state) {
            is MainViewState.LoadBooks -> {
                showBooksFragment()
            }
            is MainViewState.LoadCharacter -> {
                showCharFragment()
            }
            is MainViewState.LoadHouses -> {
                showHouseFragment()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObserver()
        if (savedInstanceState == null) {
            showCategoryFragment()
        }
    }

    private fun initObserver() {
        mainViewModel.navigationTrigger.observe(this, onViewStateChanged)
        mainViewModel.errorTrigger.observe(this, ::handlerError)
    }

    private fun handlerError(exception: Exception) =
        this.showErrorDialog(exception)

    private fun showCategoryFragment() {
        val categoryFragment = CategoryFragment.instance()
        showFragment(categoryFragment, CATEGORY_FRAGMENT_TAG)
    }

    private fun showBooksFragment() {
        val bookFragment = BookFragment.instance()
        showFragment(bookFragment, BOOK_FRAGMENT_TAG)
    }

    private fun showHouseFragment() {
        val bookFragment = HouseFragment.instance()
        showFragment(bookFragment, HOUSE_FRAGMENT_TAG)
    }

    private fun showCharFragment() {
        val bookFragment = CharacterFragment.instance()
        showFragment(bookFragment, CHAR_FRAGMENT_TAG)
    }

    private fun showFragment(fragment: Fragment, tag: String) {
        val existingFragment = supportFragmentManager.findFragmentByTag(tag)
        if (existingFragment == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment, tag).apply {
                    if (tag != CATEGORY_FRAGMENT_TAG) {
                        addToBackStack(tag)
                    }
                    commit()
                }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        private const val CATEGORY_FRAGMENT_TAG = "Tag:CategoryFragment"
        private const val BOOK_FRAGMENT_TAG = "Tag:BookFragment"
        private const val CHAR_FRAGMENT_TAG = "Tag:CharFragment"
        private const val HOUSE_FRAGMENT_TAG = "Tag:HouseFragment"
        fun createIntent(context: Context) = Intent(context, MainActivity::class.java)
    }
}
