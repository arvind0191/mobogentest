package test.arvind

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import retrofit2.Retrofit
import test.arvind.repository.network.ConnectivityMonitor
import test.arvind.repository.network.NetworkClient
import test.arvind.repository.network.NetworkMonitor
import test.arvind.sharedPrefs.SharedPrefs

class MoboApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        retrofit = NetworkClient().retrofit

        sharedPrefs = SharedPrefs(this)

        val connectivityManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityMonitor = NetworkMonitor(connectivityManager)
    }

    companion object {
        lateinit var retrofit: Retrofit
        lateinit var sharedPrefs: SharedPrefs
        lateinit var connectivityMonitor: ConnectivityMonitor
    }
}