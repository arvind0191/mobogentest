package test.arvind.repository.network.model.house

import com.google.gson.annotations.SerializedName

data class HousesItem(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("region")
    val region: String,
    @SerializedName("title")
    val title: String
)