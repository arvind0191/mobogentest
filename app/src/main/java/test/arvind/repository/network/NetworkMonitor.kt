package test.arvind.repository.network

import android.net.ConnectivityManager

interface ConnectivityMonitor {
    val isInternetAvailable: Boolean
}

class NetworkMonitor(private val connectivityManager: ConnectivityManager) : ConnectivityMonitor {
    override val isInternetAvailable: Boolean
        get() {
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }
}