package test.arvind.repository.network

sealed class ApiResult<out T> {
    class Success<T>(val value: T) : ApiResult<T>()
    class Error<T>(val exception: Exception) : ApiResult<T>()
}
