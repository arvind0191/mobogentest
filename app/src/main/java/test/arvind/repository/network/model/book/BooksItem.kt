package test.arvind.repository.network.model.book

import com.google.gson.annotations.SerializedName

data class BooksItem(
    @SerializedName("authors")
    val authors: List<String>,
    @SerializedName("country")
    val country: String,
    @SerializedName("isbn")
    val isbn: String,
    @SerializedName("mediaType")
    val mediaType: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("numberOfPages")
    val numberOfPages: Int,
    @SerializedName("publisher")
    val publisher: String,
    @SerializedName("released")
    val released: String
)