package test.arvind.repository.network

import android.accounts.NetworkErrorException
import test.arvind.MoboApplication
import test.arvind.repository.domain.Book
import test.arvind.repository.domain.Category
import test.arvind.repository.domain.Character
import test.arvind.repository.domain.House

class MoboRepository(
    private val apiService: MoboService,
    private val parser: MoboParser
) {
    suspend fun fetchCategory(): ApiResult<List<Category>>? {
        val response = apiService.getCategories()
        return if (response.isSuccessful) {
            val categoryList = parser.parseCategory(response)
            categoryList?.let {
                ApiResult.Success(it)
            }
        } else {
            ApiResult.Error(Exception(response.message()))
        }
    }

    suspend fun fetchBooks(): ApiResult<List<Book>>? {
        val response = apiService.getBooks()
        return if (response.isSuccessful) {
            val bookList = parser.parseBooks(response)
            bookList?.let {
                ApiResult.Success(it)
            }
        } else {
            ApiResult.Error(Exception(response.message()))
        }
    }

    suspend fun fetchChar(): ApiResult<List<Character>>? {
        val response = apiService.getChars()
        return if (response.isSuccessful) {
            val characterList = parser.parseCharacter(response)
            characterList?.let {
                ApiResult.Success(it)
            }
        } else {
            ApiResult.Error(Exception(response.message()))
        }
    }

    suspend fun fetchHouses(): ApiResult<List<House>>? {
        val response = apiService.getHouses()
        return if (response.isSuccessful) {
            val houseList = parser.parseHouse(response)
            houseList?.let {
                ApiResult.Success(it)
            }
        } else {
            ApiResult.Error(Exception(response.message()))
        }
    }
}

fun checkNetworkAvailability() {
    if (!MoboApplication.connectivityMonitor.isInternetAvailable) {
        throw NetworkErrorException("No Internet Available")
    }
}