package test.arvind.repository.network.model.char

import com.google.gson.annotations.SerializedName

data class CharacterItem(
    @SerializedName("aliases")
    val aliases: List<String>,
    @SerializedName("allegiances")
    val allegiances: List<String>,
    @SerializedName("born")
    val born: String,
    @SerializedName("culture")
    val culture: String,
    @SerializedName("died")
    val died: String,
    @SerializedName("father")
    val father: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("mother")
    val mother: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("playedBy")
    val playedBy: List<String>,
    @SerializedName("spouse")
    val spouse: String,
    @SerializedName("titles")
    val titles: List<String>
)