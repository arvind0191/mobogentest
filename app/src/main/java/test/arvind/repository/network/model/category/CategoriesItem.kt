package test.arvind.repository.network.model.category

import com.google.gson.annotations.SerializedName

data class CategoriesItem(
    @SerializedName("category_name")
    val category_name: String,

    @SerializedName("type")
    val type: Int
)