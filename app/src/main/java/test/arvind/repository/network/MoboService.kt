package test.arvind.repository.network

import retrofit2.Response
import retrofit2.http.GET
import test.arvind.repository.network.model.book.Books
import test.arvind.repository.network.model.category.Categories
import test.arvind.repository.network.model.char.Characters
import test.arvind.repository.network.model.house.Houses

interface MoboService {

    @GET("/categories")
    suspend fun getCategories(): Response<Categories>

    @GET("/list/1")
    suspend fun getBooks(): Response<Books>

    @GET("/list/3")
    suspend fun getChars(): Response<Characters>

    @GET("/list/2")
    suspend fun getHouses(): Response<Houses>
}
