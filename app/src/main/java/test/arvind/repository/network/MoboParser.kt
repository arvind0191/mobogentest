package test.arvind.repository.network

import com.google.gson.Gson
import retrofit2.Response
import test.arvind.repository.domain.Book
import test.arvind.repository.domain.Category
import test.arvind.repository.domain.Character
import test.arvind.repository.domain.House
import test.arvind.repository.network.model.book.Books
import test.arvind.repository.network.model.category.Categories
import test.arvind.repository.network.model.char.Characters
import test.arvind.repository.network.model.house.Houses

class MoboParser(val gson: Gson) {
    fun parseCategory(response: Response<Categories>): List<Category>? {
        return response.body()?.toDomain()
    }

    fun parseBooks(response: Response<Books>): List<Book>? {
        return response.body()?.toDomain()
    }

    fun parseHouse(response: Response<Houses>): List<House>? {
        return response.body()?.toDomain()
    }

    fun parseCharacter(response: Response<Characters>): List<Character>? {
        return response.body()?.toDomain()
    }

    private fun Categories.toDomain(): List<Category> = this.map { item ->
        Category(
            category_name = item.category_name,
            type = item.type
        )
    }

    private fun Characters.toDomain(): List<Character> = this.map { item ->
        Character(
            aliases = item.aliases,
            allegiances = item.allegiances,
            born = item.born,
            culture = item.culture,
            died = item.died,
            father = item.father,
            mother = item.mother,
            name = item.name,
            gender = item.gender,
            id = item.id,
            playedBy = item.playedBy,
            spouse = item.spouse,
            titles = item.titles
        )
    }

    private fun Houses.toDomain(): List<House> = this.map { item ->
        House(
            id = item.id,
            name = item.name,
            region = item.region,
            title = item.title
        )
    }

    private fun Books.toDomain(): List<Book> = this.map { item ->
        Book(
            authors = item.authors,
            name = item.name,
            country = item.country,
            isbn = item.isbn,
            mediaType = item.mediaType,
            numberOfPages = item.numberOfPages,
            publisher = item.publisher,
            released = item.released
        )
    }
}