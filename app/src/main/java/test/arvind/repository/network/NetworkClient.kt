package test.arvind.repository.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val DOMAIN_NAME = "https://private-anon-f69a33cb51-androidtestmobgen.apiary-mock.com"

class NetworkClient {
    private val okHttpClient = OkHttpClient()
        .newBuilder()
        .addInterceptor(createInterceptor())
        .build()

    lateinit var retrofit: Retrofit

    init {
        buildRetrofit()
    }

    private fun buildRetrofit() {
        retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(DOMAIN_NAME)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    private fun createInterceptor(): Interceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptor
    }
}