package test.arvind.repository.domain

data class Category(
    val category_name: String,
    val type: Int
)