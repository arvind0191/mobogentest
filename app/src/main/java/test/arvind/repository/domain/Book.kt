package test.arvind.repository.domain

import com.google.gson.annotations.SerializedName

data class Book(
    val authors: List<String>,
    val country: String,
    val isbn: String,
    val mediaType: String,
    val name: String,
    val numberOfPages: Int,
    val publisher: String,
    val released: String
)