package test.arvind.repository.domain

data class Character(
    val aliases: List<String>,
    val allegiances: List<String>,
    val born: String,
    val culture: String,
    val died: String,
    val father: String,
    val gender: String,
    val id: String,
    val mother: String,
    val name: String,
    val playedBy: List<String>,
    val spouse: String,
    val titles: List<String>
)