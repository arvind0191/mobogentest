package test.arvind.repository.domain

data class House(
    val id: String,
    val name: String,
    val region: String,
    val title: String
)