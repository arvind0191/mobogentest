package test.arvind

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import test.arvind.repository.network.ApiResult
import test.arvind.repository.network.MoboRepository
import test.arvind.repository.network.checkNetworkAvailability
import test.arvind.sharedPrefs.SharedPrefs
import test.arvind.feature.book.BooksViewState
import test.arvind.feature.category.Category
import test.arvind.feature.category.CategoryViewState
import test.arvind.feature.character.CharacterViewState
import test.arvind.feature.house.HouseViewState

class MainViewModel(
    private val repository: MoboRepository,
    private val sharedPrefs: SharedPrefs,
    val categoryLiveData: MutableLiveData<CategoryViewState> = MutableLiveData(),
    val bookLiveData: MutableLiveData<BooksViewState> = MutableLiveData(),
    val houseLiveData: MutableLiveData<HouseViewState> = MutableLiveData(),
    val charLiveData: MutableLiveData<CharacterViewState> = MutableLiveData(),
    val navigationTrigger: MutableLiveData<MainViewState> = MutableLiveData(),
    val errorTrigger: MutableLiveData<Exception> = MutableLiveData()
) : ViewModel() {
    internal class Factory(
        private val repository: MoboRepository,
        private val sharedPrefs: SharedPrefs
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            MainViewModel(repository, sharedPrefs) as T
                ?: throw IllegalArgumentException("this creates only MainViewModel")
    }

    fun onCategoryFragmentLoaded() {
        CoroutineScope(Dispatchers.IO).launch {
            withContext(Dispatchers.Main) {
                categoryLiveData.value = CategoryViewState.Loading
                categoryLiveData.value = CategoryViewState.Success(sharedPrefs.getCategory())
            }
        }
    }

    fun onBooksFragmentLoaded() {
        try {
            checkNetworkAvailability()
            CoroutineScope(Dispatchers.IO).launch {
                withContext(Dispatchers.Main) {
                    when (val result = repository.fetchBooks()) {
                        is ApiResult.Success -> {
                            bookLiveData.postValue(result.value?.let {
                                BooksViewState.Success(
                                    it
                                )
                            })
                        }
                        is ApiResult.Error -> {
                            errorTrigger.postValue(result.exception)
                        }
                    }
                }
            }
        } catch (exception: Exception) {
            errorTrigger.postValue(exception)
        }
    }

    fun onHouseFragmentLoaded() {
        try {
            checkNetworkAvailability()
            CoroutineScope(Dispatchers.IO).launch {
                withContext(Dispatchers.Main) {
                    when (val result = repository.fetchHouses()) {
                        is ApiResult.Success -> {
                            houseLiveData.postValue(result.value?.let {
                                HouseViewState.Success(
                                    it
                                )
                            })
                        }
                        is ApiResult.Error -> {
                            errorTrigger.postValue(result.exception)
                        }
                    }
                }
            }
        } catch (exception: Exception) {
            errorTrigger.postValue(exception)
        }
    }

    fun onCharFragmentLoaded() {
        try {
            checkNetworkAvailability()
            CoroutineScope(Dispatchers.IO).launch {
                withContext(Dispatchers.Main) {
                    when (val result = repository.fetchChar()) {
                        is ApiResult.Success -> {
                            charLiveData.postValue(result.value.let {
                                CharacterViewState.Success(
                                    it
                                )
                            })
                        }
                        is ApiResult.Error -> {
                            errorTrigger.postValue(result.exception)
                        }
                    }
                }
            }
        } catch (exception: Exception) {
            errorTrigger.postValue(exception)
        }
    }

    fun onCategoryItemClicked(categoriesItem: test.arvind.repository.domain.Category) {
        when (categoriesItem.category_name) {
            Category.BOOKS.value -> {
                navigationTrigger.postValue(MainViewState.LoadBooks)
            }
            Category.HOUSES.value -> {
                navigationTrigger.postValue(MainViewState.LoadHouses)
            }
            Category.CHARACTERS.value -> {
                navigationTrigger.postValue(MainViewState.LoadCharacter)
            }
        }
    }
}

fun getMainViewModel(
    scope: FragmentActivity,
    repository: MoboRepository,
    sharedPrefs: SharedPrefs
) =
    ViewModelProvider(
        scope,
        MainViewModel.Factory(
            repository,
            sharedPrefs
        )
    ).get(MainViewModel::class.java)