package test.arvind.sharedPrefs

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import test.arvind.repository.domain.Category

private const val STORE_FILE_NAME = "category_prefs"
private const val CATEGORY_LIST = "category_list"
private const val HAS_CATEGORY = "has_category"

class SharedPrefs(context: Context) {
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(STORE_FILE_NAME, Context.MODE_PRIVATE)
    private val editor = sharedPreferences.edit()

    fun saveCategory(categories: List<Category>) {
        editor.putString(CATEGORY_LIST, Gson().toJson(categories)).apply()
        editor.putBoolean(HAS_CATEGORY, true).apply()
    }

    fun hasCategory() = sharedPreferences.getBoolean(HAS_CATEGORY, false)

    fun getCategory(): List<Category> =
        Gson().fromJson(
            sharedPreferences.getString(CATEGORY_LIST, null),
            object : TypeToken<List<Category>>() {}.type
        )

}
