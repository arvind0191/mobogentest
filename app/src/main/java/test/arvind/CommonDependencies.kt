package test.arvind

import com.google.gson.Gson
import test.arvind.MoboApplication.Companion.retrofit
import test.arvind.repository.network.MoboParser
import test.arvind.repository.network.MoboRepository
import test.arvind.repository.network.MoboService

class CommonDependencies {
    companion object {
        private val apiServices = retrofit.create(MoboService::class.java)
        private val gson = Gson()
        private val parser = MoboParser(gson)
        val repository by lazy { MoboRepository(apiServices, parser) }
    }
}