package test.arvind

sealed class MainViewState {
    object LoadBooks : MainViewState()
    object LoadCharacter : MainViewState()
    object LoadHouses : MainViewState()
}
