package test.arvind.feature.splashscreen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import test.arvind.CommonDependencies
import test.arvind.MainActivity
import test.arvind.feature.showErrorDialog
import test.arvind.sharedPrefs.SharedPrefs

class SplashScreenActivity : AppCompatActivity() {

    private val splashScreenViewModel by lazy {
        getSplashScreenViewModel(
            this,
            CommonDependencies.repository,
            SharedPrefs(this)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        splashScreenViewModel.onSplashScreenStart()
        splashScreenViewModel.categorySavedTrigger.observe(
            this,
            { loadMainActivity() }
        )
        splashScreenViewModel.errorTrigger.observe(
            this,
            { this.showErrorDialog(it) }
        )
    }

    private fun loadMainActivity() {
        startActivity(MainActivity.createIntent(this))
        finish()
    }
}