package test.arvind.feature.splashscreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import test.arvind.repository.network.ApiResult
import test.arvind.repository.network.MoboRepository
import test.arvind.repository.network.checkNetworkAvailability
import test.arvind.sharedPrefs.SharedPrefs

class SplashScreenViewModel(
    private val repository: MoboRepository,
    private val sharedPrefs: SharedPrefs,
    val categorySavedTrigger: MutableLiveData<Unit> = MutableLiveData(),
    val errorTrigger: MutableLiveData<Exception> = MutableLiveData()
) : ViewModel() {
    internal class Factory(
        private val repository: MoboRepository,
        private val sharedPrefs: SharedPrefs
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T =
            SplashScreenViewModel(repository, sharedPrefs) as T
                ?: throw IllegalArgumentException("this creates only SplashScreenViewModel")
    }

    fun onSplashScreenStart() {
        if (sharedPrefs.hasCategory()) {
            categorySavedTrigger.postValue(Unit)
        } else {
            try {
                checkNetworkAvailability()
                CoroutineScope(Dispatchers.IO).launch {
                    withContext(Dispatchers.Main) {
                        when (val result = repository.fetchCategory()) {
                            is ApiResult.Success -> {
                                result.value.let { sharedPrefs.saveCategory(it) }
                                categorySavedTrigger.postValue(Unit)
                            }
                            is ApiResult.Error -> {
                                errorTrigger.postValue(result.exception)
                            }
                        }
                    }
                }
            } catch (exception: Exception) {
                errorTrigger.postValue(exception)
            }
        }
    }
}

fun getSplashScreenViewModel(
    scope: SplashScreenActivity,
    repository: MoboRepository,
    sharedPrefs: SharedPrefs
) =
    ViewModelProvider(
        scope,
        SplashScreenViewModel.Factory(repository, sharedPrefs)
    ).get(SplashScreenViewModel::class.java)