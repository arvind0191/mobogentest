package test.arvind.feature.house

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import test.arvind.R
import test.arvind.repository.domain.House

class HouseViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {
    private val name = itemView.findViewById<TextView>(R.id.houseName)
    private val image = itemView.findViewById<ImageView>(R.id.houseImage)
    private val description = itemView.findViewById<TextView>(R.id.houseDescription)
    fun bindView(housesItem: House) {
        name.text = housesItem.name
        description.text = housesItem.title
        val houseImageUrl = housesItem.region.toRegionalImageSrc()
        if (houseImageUrl == null) {
            image.visibility = View.GONE
        } else {
            image.visibility = View.VISIBLE
            Glide.with(itemView)
                .load(houseImageUrl)
                .into(image)
        }
    }
}

private fun String.toRegionalImageSrc(): String? =
    when (this) {
        Region.THE_NORTH.value -> HouseImageSrc.the_north_image_url
        Region.THE_IRON_ISLANDS.value -> HouseImageSrc.the_riverlands_or_iron_islands_image_url
        Region.THE_RIVERLANDS.value -> HouseImageSrc.the_riverlands_or_iron_islands_image_url
        Region.THE_STORMLANDS.value -> HouseImageSrc.the_stormlands_image_url
        Region.THE_VALE.value -> HouseImageSrc.the_vale_image_url
        Region.THE_REACH.value -> HouseImageSrc.the_reach_image_url
        Region.THE_WESTERLANDS.value -> HouseImageSrc.the_westerlands_image_url
        Region.DORNE.value -> HouseImageSrc.dorne_image_url
        else -> null
    }
