package test.arvind.feature.house

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import test.arvind.CommonDependencies
import test.arvind.MoboApplication
import test.arvind.R
import test.arvind.getMainViewModel
import test.arvind.repository.domain.House
import test.arvind.MainActivity
import test.arvind.feature.showActionBar

class HouseFragment : Fragment(R.layout.fragment_house) {
    private val mainViewModel by lazy {
        getMainViewModel(
            requireActivity(),
            CommonDependencies.repository,
            MoboApplication.sharedPrefs
        )
    }

    private val houseAdapter by lazy { HouseAdapter() }

    private var progressBar: ProgressBar? = null

    private val onViewStateChanged = Observer<HouseViewState> { state ->
        when (state) {
            is HouseViewState.Loading -> showLoading()
            is HouseViewState.Success -> {
                hideLoading()
                loadHousesList(state.houses)
            }
        }
    }

    private fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    private fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    private fun loadHousesList(houses: List<House>) {
        houseAdapter.setHousesList(houses)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressbar)
        (requireActivity() as MainActivity).showActionBar(true, R.string.house_title)
        initHouseList(view)
        initHouseObserver()
        mainViewModel.onHouseFragmentLoaded()
    }

    private fun initHouseObserver() {
        mainViewModel.houseLiveData.observe(requireActivity(), onViewStateChanged)
    }

    private fun initHouseList(view: View) {
        view.findViewById<RecyclerView>(R.id.house_recyclerview)?.let { recyclerView ->
            with(recyclerView) {
                adapter = houseAdapter
                layoutManager = LinearLayoutManager(requireActivity())
            }
        }
    }

    companion object {
        fun instance() = HouseFragment()
    }
}