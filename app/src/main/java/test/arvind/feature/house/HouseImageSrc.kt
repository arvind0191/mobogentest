package test.arvind.feature.house

class HouseImageSrc {
    companion object {
        const val the_north_image_url: String =
            "https://awoiaf.westeros.org/images/thumb/7/7e/House_Stark.svg/200px-House_Stark.svg.png"
        const val the_vale_image_url: String =
            "https://awoiaf.westeros.org/images/thumb/b/b4/House_Arryn.svg/200px-House_Arryn.svg.png"
        const val the_westerlands_image_url: String =
            "https://awoiaf.westeros.org/images/thumb/d/d5/House_Lannister.svg/200px-House_Lannister.svg.png"
        const val the_riverlands_or_iron_islands_image_url: String =
            "https://awoiaf.westeros.org/images/thumb/d/d1/House_Hoare.svg/200px-House_Hoare.svg.png"
        const val the_reach_image_url: String =
            "https://awoiaf.westeros.org/images/thumb/f/f2/House_Gardener.svg/200px-House_Gardener.svg.png"
        const val dorne_image_url: String =
            "https://awoiaf.westeros.org/images/thumb/6/60/House_Martell.svg/200px-House_Martell.svg.png"
        const val the_stormlands_image_url: String =
            "https://awoiaf.westeros.org/images/thumb/2/2d/House_Baratheon.svg/200px-House_Baratheon.svg.png"
    }
}

