package test.arvind.feature.house

import test.arvind.repository.domain.House

sealed class HouseViewState {
    class Success(val houses: List<House>) : HouseViewState()
    object Loading : HouseViewState()
}