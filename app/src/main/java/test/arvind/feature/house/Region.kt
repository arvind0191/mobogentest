package test.arvind.feature.house

enum class Region(val value: String) {
    THE_NORTH("The North"),
    THE_VALE("The Vale"),
    THE_RIVERLANDS("The Riverlands"),
    THE_IRON_ISLANDS("Iron Islands"),
    THE_WESTERLANDS("The Westerlands"),
    THE_REACH("The Reach"),
    DORNE("Dorne"),
    THE_STORMLANDS("The Stormlands")
}
