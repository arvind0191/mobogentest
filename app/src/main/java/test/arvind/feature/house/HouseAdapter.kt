package test.arvind.feature.house

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import test.arvind.R
import test.arvind.repository.domain.House
import test.arvind.feature.inflateView

class HouseAdapter : RecyclerView.Adapter<HouseViewHolder>() {

    private var housesList = emptyList<House>()

    fun setHousesList(houses: List<House>) {
        this.housesList = houses
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        HouseViewHolder(parent.inflateView(R.layout.item_houses))

    override fun onBindViewHolder(holder: HouseViewHolder, position: Int) {
        holder.bindView(housesList[position])
    }

    override fun getItemCount() = housesList.size
}