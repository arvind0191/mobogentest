package test.arvind.feature.category

enum class Category(val value: String) {
    BOOKS("Books"),
    HOUSES("Houses"),
    CHARACTERS("Characters")
}
