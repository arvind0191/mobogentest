package test.arvind.feature.category

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import test.arvind.R
import test.arvind.repository.domain.Category
import test.arvind.feature.inflateView

class CategoryAdapter(
    private val onCategoryItemClicked: (Category) -> Unit
) : RecyclerView.Adapter<CategoryViewHolder>() {

    private var categoryList = emptyList<Category>()

    fun setCategoryItem(categories: List<Category>) {
        this.categoryList = categories.sortedBy { it.category_name }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoryViewHolder(parent.inflateView(R.layout.item_category), onCategoryItemClicked)


    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bindView(categoryList[position])
    }

    override fun getItemCount() = categoryList.size
}