package test.arvind.feature.category

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import test.arvind.CommonDependencies
import test.arvind.MoboApplication
import test.arvind.R
import test.arvind.getMainViewModel
import test.arvind.repository.domain.Category
import test.arvind.MainActivity
import test.arvind.feature.showActionBar

class CategoryFragment : Fragment(R.layout.fragment_category) {
    private val mainViewModel by lazy {
        getMainViewModel(
            requireActivity(),
            CommonDependencies.repository,
            MoboApplication.sharedPrefs
        )
    }

    private var progressBar: ProgressBar? = null

    private val categoryAdapter by lazy {
        CategoryAdapter(
            onCategoryItemClicked = { mainViewModel.onCategoryItemClicked(it) }
        )
    }

    private val onViewStateChanged = Observer<CategoryViewState> { state ->
        when (state) {
            is CategoryViewState.Loading -> showLoading()
            is CategoryViewState.Success -> {
                hideLoading()
                loadTheCategoryList(state.categories)
            }
        }
    }

    private fun loadTheCategoryList(categories: List<Category>) =
        categoryAdapter.setCategoryItem(categories)

    private fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressbar)
        (requireActivity() as MainActivity).showActionBar(false, R.string.category_title)
        initCategoryList(view)
        initCategoryObservers()
        mainViewModel.onCategoryFragmentLoaded()
    }

    private fun initCategoryList(view: View) {
        view.findViewById<RecyclerView>(R.id.category_recyclerview)?.let { recyclerView ->
            with(recyclerView) {
                adapter = categoryAdapter
                layoutManager = LinearLayoutManager(requireActivity())
            }
        }
    }

    private fun initCategoryObservers() {
        mainViewModel.categoryLiveData.observe(requireActivity(), onViewStateChanged)
    }

    companion object {
        fun instance() = CategoryFragment()
    }
}