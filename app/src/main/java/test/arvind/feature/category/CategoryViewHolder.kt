package test.arvind.feature.category

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import test.arvind.R
import test.arvind.repository.domain.Category

class CategoryViewHolder(
    itemView: View,
    private val onItemClicked: (Category) -> Unit
) : RecyclerView.ViewHolder(itemView) {
    private val categoryName = itemView.findViewById<TextView>(R.id.categoryName)
    fun bindView(categoriesItem: Category) {
        categoryName.text = categoriesItem.category_name
        itemView.setOnClickListener { onItemClicked(categoriesItem) }
    }
}
