package test.arvind.feature.category

import test.arvind.repository.domain.Category

sealed class CategoryViewState {
    class Success(val categories: List<Category>) : CategoryViewState()
    object Loading : CategoryViewState()
}
