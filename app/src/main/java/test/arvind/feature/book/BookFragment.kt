package test.arvind.feature.book

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import test.arvind.CommonDependencies
import test.arvind.MoboApplication
import test.arvind.R
import test.arvind.getMainViewModel
import test.arvind.repository.domain.Book
import test.arvind.MainActivity
import test.arvind.feature.showActionBar

class BookFragment : Fragment(R.layout.fragment_book) {
    private val mainViewModel by lazy {
        getMainViewModel(
            requireActivity(),
            CommonDependencies.repository,
            MoboApplication.sharedPrefs
        )
    }
    private val booksAdapter by lazy { BookAdapter() }

    private var progressBar: ProgressBar? = null

    private val onViewStateChanged = Observer<BooksViewState> { state ->
        when (state) {
            is BooksViewState.Loading -> showLoading()
            is BooksViewState.Success -> {
                hideLoading()
                loadBooksList(state.books)
            }
        }
    }

    private fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    private fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    private fun loadBooksList(books: List<Book>) {
        booksAdapter.setBooksList(books)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressbar)
        (requireActivity() as MainActivity).showActionBar(true, R.string.books_title)
        initBooksList(view)
        initBooksObservers()
        mainViewModel.onBooksFragmentLoaded()
    }

    private fun initBooksObservers() {
        mainViewModel.bookLiveData.observe(requireActivity(), onViewStateChanged)
    }

    private fun initBooksList(view: View) {
        view.findViewById<RecyclerView>(R.id.books_recyclerview)?.let { recyclerView ->
            with(recyclerView) {
                adapter = booksAdapter
                layoutManager = LinearLayoutManager(requireActivity())
            }
        }
    }

    companion object {
        fun instance() = BookFragment()
    }
}