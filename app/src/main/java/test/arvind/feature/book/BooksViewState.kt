package test.arvind.feature.book

import test.arvind.repository.domain.Book

sealed class BooksViewState {
    class Success(val books: List<Book>) : BooksViewState()
    object Loading : BooksViewState()
}