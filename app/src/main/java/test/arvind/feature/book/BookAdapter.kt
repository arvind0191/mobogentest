package test.arvind.feature.book

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import test.arvind.R
import test.arvind.repository.domain.Book
import test.arvind.feature.inflateView

class BookAdapter : RecyclerView.Adapter<BooksViewHolder>() {

    private var bookList = emptyList<Book>()

    fun setBooksList(books: List<Book>) {
        this.bookList = books
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        BooksViewHolder(parent.inflateView(R.layout.item_books))

    override fun onBindViewHolder(holder: BooksViewHolder, position: Int) {
        holder.bindView(bookList[position])
    }

    override fun getItemCount() = bookList.size
}