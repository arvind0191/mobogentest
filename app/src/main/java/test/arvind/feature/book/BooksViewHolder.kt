package test.arvind.feature.book

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import test.arvind.R
import test.arvind.repository.domain.Book
import test.arvind.feature.toMultilineText

class BooksViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {
    private val name = itemView.findViewById<TextView>(R.id.booksName)
    private val country = itemView.findViewById<TextView>(R.id.country)
    private val numberOfPages = itemView.findViewById<TextView>(R.id.numberOfPages)
    private val isbn = itemView.findViewById<TextView>(R.id.isbn)
    private val mediaType = itemView.findViewById<TextView>(R.id.mediaType)
    private val publisher = itemView.findViewById<TextView>(R.id.publisher)
    private val released = itemView.findViewById<TextView>(R.id.released)
    private val authorList = itemView.findViewById<TextView>(R.id.authors)
    fun bindView(booksItem: Book) {
        name.text = "Name - ${booksItem.name}"
        country.text = "Country - ${booksItem.country}"
        numberOfPages.text = "Pages - ${booksItem.numberOfPages.toString()}"
        isbn.text = "isBn - ${booksItem.isbn}"
        mediaType.text = "Type - ${booksItem.mediaType}"
        publisher.text = "Publisher - ${booksItem.publisher}"
        released.text = "Release date - ${booksItem.released}"
        authorList.text = "Authors - ${booksItem.authors.toMultilineText()}"
    }
}
