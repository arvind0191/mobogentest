package test.arvind.feature

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import test.arvind.MainActivity
import test.arvind.R

fun ViewGroup.inflateView(@LayoutRes layoutId: Int): View =
    LayoutInflater.from(context).inflate(layoutId, this, false)

fun MainActivity.showActionBar(hasBackButton: Boolean, @StringRes titleRes: Int) {
    this.supportActionBar?.setDisplayHomeAsUpEnabled(hasBackButton)
    this.title = getString(titleRes)
}

fun List<String>.toMultilineText() = this.joinToString(separator = "\n      ")

fun Activity.showErrorDialog(exception: Exception) {
    AlertDialog.Builder(this)
        .setTitle(R.string.error_title)
        .setMessage(exception.message)
        .setPositiveButton(R.string.general_close) { dialogInterface, _ ->
            dialogInterface.dismiss()
            finish()
        }
        .show()
}