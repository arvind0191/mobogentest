package test.arvind.feature.character

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import test.arvind.R
import test.arvind.repository.domain.Character
import test.arvind.feature.inflateView

class CharacterAdapter : RecyclerView.Adapter<CharacterViewHolder>() {

    private var characterList = emptyList<Character>()

    fun setCharList(chars: List<Character>) {
        this.characterList = chars
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CharacterViewHolder(parent.inflateView(R.layout.item_chars))

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bindView(characterList[position])
    }

    override fun getItemCount() = characterList.size
}