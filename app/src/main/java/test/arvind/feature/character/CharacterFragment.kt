package test.arvind.feature.character

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import test.arvind.CommonDependencies
import test.arvind.MoboApplication
import test.arvind.R
import test.arvind.getMainViewModel
import test.arvind.repository.domain.Character
import test.arvind.MainActivity
import test.arvind.feature.showActionBar

class CharacterFragment : Fragment(R.layout.fragment_char) {
    private val mainViewModel by lazy {
        getMainViewModel(
            requireActivity(),
            CommonDependencies.repository,
            MoboApplication.sharedPrefs
        )
    }

    private var progressBar: ProgressBar? = null

    private val charAdapter by lazy { CharacterAdapter() }

    private val onViewStateChanged = Observer<CharacterViewState> { state ->
        when (state) {
            is CharacterViewState.Loading -> showLoading()
            is CharacterViewState.Success -> {
                hideLoading()
                loadTheCharList(state.char)
            }
        }
    }

    private fun loadTheCharList(chars: List<Character>) = charAdapter.setCharList(chars)

    private fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressbar)
        (requireActivity() as MainActivity).showActionBar(true, R.string.character_title)
        initCharList(view)
        initCharObservers()
        mainViewModel.onCharFragmentLoaded()
    }

    private fun initCharObservers() {
        mainViewModel.charLiveData.observe(requireActivity(), onViewStateChanged)
    }

    private fun initCharList(view: View) {
        view.findViewById<RecyclerView>(R.id.char_recyclerview)?.let { recyclerView ->
            with(recyclerView) {
                adapter = charAdapter
                layoutManager = LinearLayoutManager(requireActivity())
            }
        }
    }

    companion object {
        fun instance() = CharacterFragment()
    }
}