package test.arvind.feature.character

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import test.arvind.R
import test.arvind.repository.domain.Character
import test.arvind.feature.toMultilineText

class CharacterViewHolder(
    itemView: View,
) : RecyclerView.ViewHolder(itemView) {
    private val name = itemView.findViewById<TextView>(R.id.nameChar)
    private val gender = itemView.findViewById<TextView>(R.id.genderChar)
    private val culture = itemView.findViewById<TextView>(R.id.cultureChar)
    private val born = itemView.findViewById<TextView>(R.id.bornChar)
    private val died = itemView.findViewById<TextView>(R.id.diedChar)
    private val titles = itemView.findViewById<TextView>(R.id.titlesChar)
    private val aliases = itemView.findViewById<TextView>(R.id.aliasChar)
    private val fatherName = itemView.findViewById<TextView>(R.id.fatherNameChar)
    private val motherName = itemView.findViewById<TextView>(R.id.motherNameChar)
    private val spouseName = itemView.findViewById<TextView>(R.id.spouseNameChar)
    private val allegiances = itemView.findViewById<TextView>(R.id.allegiancesChar)
    private val playedBy = itemView.findViewById<TextView>(R.id.playedByChar)

    fun bindView(charItem: Character) {
        name.text = "Name - ${charItem.name}"
        gender.text = "Gender - ${charItem.gender}"
        culture.text = "Culture - ${charItem.culture}"
        born.text = "Born - ${charItem.born}"
        died.text = "Died - ${charItem.died}"
        titles.text = "Titles - ${charItem.titles.toMultilineText()}"
        aliases.text = "Aliases - ${charItem.aliases.toMultilineText()}"
        fatherName.text = "Father - ${charItem.father}"
        motherName.text = "Mother - ${charItem.mother}"
        spouseName.text = "Spouse - ${charItem.spouse}"
        allegiances.text = "Allegiances - ${charItem.allegiances.toMultilineText()}"
        playedBy.text = "Played By - ${charItem.playedBy.toMultilineText()}"
    }
}
