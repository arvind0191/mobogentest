package test.arvind.feature.character

import test.arvind.repository.domain.Character

sealed class CharacterViewState {
    class Success(val char: List<Character>) : CharacterViewState()
    object Loading : CharacterViewState()
}