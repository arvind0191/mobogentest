package test.arvind.feature.main

import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.filters.LargeTest
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import test.arvind.MainActivity
import test.arvind.R

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = IntentsTestRule(MainActivity::class.java)

    @Test
    fun onLaunchOfAppMainScreenShouldDisplayListOfCategory() {
        val categoryList = listOf("Books", "Characters", "Houses")
        moboCategory {
            R.id.category_recyclerview.isDisplayed()
            R.id.category_recyclerview.shouldHaveTextAtPosition(categoryList[0], 0)
            R.id.category_recyclerview.shouldHaveTextAtPosition(categoryList[1], 1)
            R.id.category_recyclerview.shouldHaveTextAtPosition(categoryList[2], 2)
        }
    }

    @Test
    fun onBookClickedScreenShouldLoadBookList() {
        moboCategory {
            R.id.category_recyclerview.isDisplayed()
            clickOnBook()
            R.id.books_recyclerview.isDisplayed()
        }
    }
}
