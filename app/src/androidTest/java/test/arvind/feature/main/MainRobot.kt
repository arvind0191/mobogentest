package test.arvind.feature.main

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.*
import test.arvind.R

inline fun moboCategory(block: MainRobot.() -> Unit) = MainRobot().apply(block)
class MainRobot {

    fun Int.shouldHaveTextAtPosition(text: String, position: Int) {
        onView(withId(R.id.category_recyclerview))
            .perform(actionOnItemAtPosition<RecyclerView.ViewHolder>(position, scrollTo()))
            .check(matches(hasDescendant(withText(text))))
    }

    fun clickOnBook() {
        onView(withId(R.id.category_recyclerview))
            .check(matches(hasDescendant(withText("Books"))))
    }
}